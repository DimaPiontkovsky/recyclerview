package com.example.newapp.data.recycler;

import androidx.annotation.NonNull;

public class  Item {
    private int id;
    private String firstName;
    private String secondName;

    public Item(int id, String firstName, String secondName) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @NonNull
    @Override
    public String toString() {
        return ""+id;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
