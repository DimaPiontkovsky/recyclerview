package com.example.newapp.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newapp.data.recycler.Item;
import com.example.newapp.databinding.ItemBinding;
import com.example.newapp.presentation.activity.recycleractivity.IRecyclerView;
import com.example.newapp.presentation.activity.recycleractivity.RecyclerPresenter;


public class MyRecyclerHolder extends RecyclerView.ViewHolder {
private IRecyclerView.Presenter presenter;
private Context context;
private ItemBinding binding;


    public MyRecyclerHolder(@NonNull View itemView, Context context, IRecyclerView.Presenter presenter) {
        super(itemView);
        this.context = context;
        this.presenter = presenter;
        binding = DataBindingUtil.bind(itemView);
    }

    public void bind(int position, Item item){
        if (item != null && item.getFirstName() != null && !item.getFirstName().isEmpty()){
            binding.setItem(item);
            binding.cLayout.setOnLongClickListener(v ->{
                presenter.delete(position);
                return false;
            });
        }
    }
}
