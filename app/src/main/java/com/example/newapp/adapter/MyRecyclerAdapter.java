package com.example.newapp.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newapp.R;
import com.example.newapp.data.recycler.Item;
import com.example.newapp.presentation.activity.recycleractivity.IRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerHolder> {
    IRecyclerView.Presenter presenter;
    private List<Item> list;


    public MyRecyclerAdapter(List<Item> list, IRecyclerView.Presenter presenter) {
        this.presenter = presenter;
        if (list == null) {
            list = new ArrayList<>();
        }
        this.list = list;
    }

    @NonNull
    @Override
    public MyRecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecyclerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false),parent.getContext(), presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecyclerHolder holder, int position) {
        holder.bind(position, list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void delete(int position) {
        list.remove(position);
        notifyDataSetChanged();
    }

    public void read(List<Item> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
