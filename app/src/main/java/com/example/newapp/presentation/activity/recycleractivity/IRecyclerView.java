package com.example.newapp.presentation.activity.recycleractivity;

import com.example.newapp.data.recycler.Item;
import com.example.newapp.presentation.base.BasePresenter;

import java.util.List;

public interface IRecyclerView {
    interface View {
        void initAdapter(List<Item> list);

        void read(List<Item> list);

        void delete(int position);
        void onClick(int pos);

    }

    interface Presenter extends BasePresenter<View> {
        void onClick();

        void init();

        void delete(int position);

        void read();

    }
}
