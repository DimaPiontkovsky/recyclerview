package com.example.newapp.presentation.activity.recycleractivity;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.newapp.R;
import com.example.newapp.adapter.MyRecyclerAdapter;
import com.example.newapp.data.recycler.Item;
import com.example.newapp.databinding.ActivityRecyclerBinding;
import com.example.newapp.presentation.base.BaseActivity;
import com.example.newapp.presentation.base.BasePresenter;

import java.util.List;

public class RecyclerActivity extends BaseActivity<ActivityRecyclerBinding> implements IRecyclerView.View {
    private MyRecyclerAdapter adapter;
private IRecyclerView.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
presenter = new RecyclerPresenter();
getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_recycler;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
        presenter.init();
    }

    @Override
    protected void stopView() { presenter.onStopView();

    }

    @Override
    public void initAdapter(List<Item> list) {
        adapter = new MyRecyclerAdapter(list, presenter);
        getBinding().rvExample.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBinding().rvExample.setAdapter(adapter);
    }

    @Override
    public void read(List<Item> list) {
            adapter.read(list);
    }

    @Override
    public void delete(int position) {
        adapter.delete(position);
    }

    @Override
    public void onClick(int pos) {

    }
}