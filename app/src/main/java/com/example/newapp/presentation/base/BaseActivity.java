package com.example.newapp.presentation.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        initView(savedInstanceState);
    }

    protected abstract void initView(@Nullable Bundle savedInstanceState);

    @LayoutRes

    protected abstract int getLayoutRes();

    protected abstract BasePresenter getPresenter();

    protected abstract void startView();

    protected abstract void stopView();

    protected Binding getBinding(){
     return binding;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (getPresenter() != null){
            getPresenter().onStopView();
        }
        super.onDestroy();
    }
    protected <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T object){
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}