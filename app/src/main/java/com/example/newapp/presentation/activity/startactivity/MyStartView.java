package com.example.newapp.presentation.activity.startactivity;

import com.example.newapp.presentation.base.BasePresenter;

public interface MyStartView {
    interface View{
    }

    interface Presenter extends BasePresenter<MyStartView.View> {
        void onClick();

    }
}
