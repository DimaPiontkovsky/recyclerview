package com.example.newapp.presentation.activity.startactivity;


public class MyStartPresenter implements MyStartView.Presenter {
    private MyStartView.View view;

    public MyStartPresenter() {

    }

    @Override
    public void onStartView(MyStartView.View view) {
        this.view = view;

    }

    @Override
    public void onClick() {
    }

    @Override
    public void onStopView() {
        if (view != null) view = null;;

    }


}
