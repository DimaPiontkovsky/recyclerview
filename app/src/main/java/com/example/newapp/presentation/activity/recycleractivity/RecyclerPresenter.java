package com.example.newapp.presentation.activity.recycleractivity;

import androidx.recyclerview.widget.RecyclerView;

import com.example.newapp.data.recycler.Item;

import java.util.ArrayList;
import java.util.List;

public class RecyclerPresenter implements  IRecyclerView.Presenter {
    private IRecyclerView.View view;

    public RecyclerPresenter(){

    }



    @Override
    public void onStartView(IRecyclerView.View view) {
        this.view = view;

    }

    @Override
    public void onClick() {view.read(mock());    }

        private List<Item> mock(){
            List<Item> list = new ArrayList<>();
            list.add(new Item(1,"Ivan","Batman"));
            list.add(new Item(2,"Vasiliy","SpiderMan"));
            list.add(new Item(3,"Petro","SuperMan"));
            list.add(new Item(4,"Nikolay","IronMan"));
            list.add(new Item(5,"Igor","Thor"));
            list.add(new Item(6,"Andriy","Hulk"));
            list.add(new Item(7,"Anna","SuperGirl"));
            list.add(new Item(8,"Lucy","SuperWoman"));
            list.add(new Item(9,"Victoria","CatWoman"));
            list.add(new Item(10,"Iren","Wasp"));

            return list;
        }



    @Override
    public void init() {
view.initAdapter(mock());

    }

    @Override
    public void delete(int position) {
        view.delete(position);

    }

    @Override
    public void read() {
        view.read(mock());

    }

    @Override
    public void onStopView() { view = null;

    }
}
